<?php

abstract class Country 
{
    protected $slogan;

    abstract public function sayHello();

    public function setSlogan(string $slogan) 
    {
        $this->slogan = $slogan;
    }

    public function getSlogan()
    {
        return $this->slogan;
    }
}

interface Boss 
{
    public function checkValidSlogan();
}

class EnglandCountry extends Country implements Boss
{
    public function sayHello()
    {
        echo 'Hello';
    }

    public function checkValidSlogan()
    {
        $str = strtolower($this->slogan);
        return strpos($str, 'england') !== false || strpos($str, 'english') !== false;
    }
}

class VietnamCountry extends Country implements Boss
{
    public function sayHello()
    {
        echo 'Xin chao';
    }

    public function checkValidSlogan()
    {
        $str = strtolower($this->slogan);
        return strpos($str, 'vietnam') !== false && strpos($str, 'hust') !== false;
    }
}

$englandCountry = new EnglandCountry();
$vietnamCountry = new VietnamCountry();

$englandCountry->setSlogan('England is a country that is part of the United Kingdom. It shares land borders with Wales to the west and Scotland to the north. The Irish Sea lies west of England and the Celtic Sea to the southwest.');

$vietnamCountry->setSlogan('Vietnam is the easternmost country on the Indochina Peninsula. With an estimated 94.6 million inhabitants as of 2016, it is the 15th most populous country in the world.');

$englandCountry->sayHello();
echo "\n";
$vietnamCountry->sayHello();
echo "\n";
var_dump($englandCountry->checkValidSlogan());
var_dump($vietnamCountry->checkValidSlogan());
