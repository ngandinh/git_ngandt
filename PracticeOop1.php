<?php

function countSentences($str) {
    $count = preg_match_all('/[^\s](\.|\!|\?)(?!\w)/', $str, $match);
    if ($count > 0) {
        return $count;
    } else {
        return 0;
    }
}

class ExerciseString 
{
    public $check1;
    public $check2;
    
    public function readFile($file) 
    {
        $fp = fopen($file, "r");
        if ($fp != null) {
            return $get = fgets($fp);
        } else return 0;
    }

    public function checkValidString($get) 
    {
        $sub1 = 'book';
        $sub2 = 'restaurant';
        $pos1 = strpos($get, $sub1);
        $pos2 = strpos($get, $sub2);
        return $pos1 xor $pos2;
    }

    public function writeFile($file1, $file2) 
    {
        $fp = fopen('result.txt', "w");

        if ($this->check1 == true) {
            fwrite($fp, 'Check1 la chuoi "Hop le". Chuoi co ' . countSentences($file1) . " cau\n");
        } else {
            fwrite($fp, 'Check1 la chuoi "Khong hop le". Chuoi co ' . countSentences($file1) . " cau\n");
        }

        if ($this->check2 == true) {
            fwrite($fp, 'Check2 la chuoi "Hop le". Chuoi co ' . countSentences($file2)  . " cau\n");
        } else {
            fwrite($fp, 'Check2 la chuoi "Khong hop le". Chuoi co ' . countSentences($file2) . " cau\n");
        }
        fclose($fp);
    }
}

$object1 = new ExerciseString();
$c1 = $object1->readFile('file1.txt');
$c2 = $object1->readFile('file2.txt');

if($c1 == null || $c2 == null) {
    echo 'Cannot open file!';
    return 0;
} else {
    $object1->check1 = $object1->checkValidString($c1);
    $object1->check2 = $object1->checkValidString($c2);
    $object1->writeFile($c1, $c2);
}
