CREATE TABLE `categories` 
(
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `items` 
(
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `amount` int(10) NOT NULL,
  `category_id` int(10) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `categories`
    (`id`, `name`, `description`)
VALUES
    (1, "SC", "Sample Category"),
    (2, "OSC", "Other Sample Category"),
    (3, "EC", "Empty Category");

INSERT INTO `items`
    (`id`, `name`, `amount`, `category_id`)
VALUES
    (1, "Item 1", 10, 1),
    (2, "Item 2", 20, 1),
    (3, "Item 3", 30, 2),
    (4, "Item 4", 40, 2),
    (5, "Item 5", 50);

/* Cau 1 */
SELECT 
    c.id, 
    c.name, 
    c.description, 
    c.created_at, 
    c.updated_at, 
    count(i.category_id) AS "Num of items" 
FROM categories c 
    LEFT JOIN items i 
    ON c.id = i.category_id 
    GROUP BY c.id;

/* Cau 2 */
SELECT 
    c.id, 
    c.name, 
    c.description, 
    c.created_at, 
    c.updated_at, 
    sum(i.amount) AS "Amount items" 
FROM 
    categories c 
    LEFT JOIN items i 
    ON c.id = i.category_id 
    GROUP BY c.id;

/* Cau 3 */
SELECT 
    c.id, 
    c.name, 
    c.description, 
    c.created_at, 
    c.updated_at 
FROM 
    categories c 
    JOIN items i 
    ON c.id = i.category_id 
    WHERE i.amount > 40 
    GROUP BY c.id 
    HAVING count(i.category_id) >= 1;

/* Cau 4 */
DELETE 
    categories 
FROM 
    categories 
    LEFT JOIN items 
    ON categories.id = items.category_id 
    WHERE category_id IS NULL;
